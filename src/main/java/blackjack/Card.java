package blackjack;



/**
 *
 * @author akret
 */
public class Card {

   private Suit suit; 
   private Value value;

   //Constructor
   public Card(Suit suit, Value value){
       this.suit = suit;
       this.value = value;
   }   

   
   //getter for checking values
   public Value getValue(){
       return this.value;
   }
   
    @Override
   public String toString(){
       return this.value + " of " + this.suit;
   }
}
   
   
    

