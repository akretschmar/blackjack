
package blackjack;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author akret
 */
public class Shuffle extends Deck{
     @Override
    public void shuffle(){
        ArrayList<Card> tempDeck = new ArrayList<>();
        Random random = new Random();
        int randomCard = 0;
        int deckSize = this.cards.size();
        
        for(int i = 0; i < deckSize; i++){
            randomCard = random.nextInt((this.cards.size()-1 - 0) + 1) + 0;
            tempDeck.add(this.cards.get(randomCard));
            this.cards.remove(randomCard);
        }
        this.cards = tempDeck;
        
    }    
}
