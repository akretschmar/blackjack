
package blackjack;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author akret
 */
public class Deck {
    protected ArrayList<Card> cards;
    
    //Constructor
    public Deck(){
        this.cards = new ArrayList<Card>();
    }
    //createDeck() will create a full deck of 52 cards from TWO to ACE for all 4 suits
    public void createDeck(){
        for(Suit cardSuit : Suit.values()){
            for(Value cardValue : Value.values()){
                this.cards.add(new Card(cardSuit, cardValue));
            }
        }
    }
    
    //remove get and add cards
    public void removeCard(int i){
           this.cards.remove(i);
    }
    
    public Card getCard(int i){
        return this.cards.get(i);
    }
    
    public void addCard(Card addACard){
        this.cards.add(addACard);
    }
    
       public void shuffle(){}   
       
    //removes from one deck and adds to another
    public void draw(Deck draw){
        this.cards.add(draw.getCard(0));
        draw.removeCard(0);
    }
    public int deckSize(){
        return this.cards.size();
    }
    //shows the value of the cards in hand
    public int showValue(){
        int showValue = 0;
        int aces = 0;
        
        for(Card aCard : this.cards){
            switch(aCard.getValue()){
                case TWO: showValue += 2; break;
                case THREE: showValue += 3; break;
                case FOUR: showValue += 4; break;
                case FIVE: showValue += 5; break;
                case SIX: showValue += 6; break;
                case SEVEN: showValue += 7; break;
                case EIGHT: showValue += 8; break;
                case NINE: showValue += 9; break;
                case TEN: showValue += 10; break;
                case JACK: showValue += 10; break;
                case QUEEN: showValue += 10; break;
                case KING: showValue += 10; break;
                case ACE: aces += 1; break;
            }
        }
        
        
        //makes sure the aces value is 1 or 11
        for(int i = 0; i < aces ; i++){
            if(showValue>10){
                showValue += 1;
            }
            else{
                showValue += 11;
            }
        }
        return showValue;
    }
     
    public void moveCardsBackToDeck(Deck move){
        int thisDeckSize = this.cards.size();
        for(int i = 0; i < thisDeckSize; i++){
            move.addCard(this.getCard(i));
        }
        for(int i = 0; i < thisDeckSize; i++){
            this.removeCard(0);
        }
    }
    @Override
    public String toString(){
        String cardList = "";
        for(Card singleCard: this.cards){
            cardList += "\n" + singleCard;             
        }
        return cardList;
    }
    
}
