
package blackjack;

/**
 *
 * @author akret
 */
public class Hand extends Deck{
    
   @Override
    public void draw(Deck draw){
        this.cards.add(draw.getCard(0));
        draw.removeCard(0);
    }
    
   @Override
    public void removeCard(int i){
           this.cards.remove(i);
    }
    
   @Override
    public Card getCard(int i){
        return this.cards.get(i);
    }
    
   @Override
    public void addCard(Card addACard){
        this.cards.add(addACard);
    }
}
