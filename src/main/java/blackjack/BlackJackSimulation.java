package blackjack;

import java.util.Random;
import java.util.Scanner;


/**
 *
 * @author akret
 */
public class BlackJackSimulation {

    public static void main(String[] args)
    {
        System.out.println("Let's play some BlackJack!");
        System.out.println("The rules are simple, don't go over 21!");
        Scanner kb = new Scanner(System.in);
        
        Deck deck = new Deck();
        deck.createDeck();
        deck.shuffle();
        
        
        //create decks for player and dealer
        Deck playersHand = new Hand();     
        Deck dealersHand = new Hand();
        
        //Money for the player to start with
        int money = 50;
        
        //Start the games loop
        while(money>0){
            if(money>200){
                System.out.println("You have too much money for this table! You Win!!");
                break;
            }
            System.out.println("You have $" + money + " How much would you like to bet?");
            int playerBet = kb.nextInt();
            if(playerBet > money){            
                System.out.println("You can't bet more than you have, start over!");
                break;
            }
            
            boolean endofTheRound = false;
            
            //player and dealer draw two cards each
            playersHand.draw(deck);
            dealersHand.draw(deck);
            playersHand.draw(deck);
            dealersHand.draw(deck);
            
            
            while(true){
                //print the players hand
                System.out.println("Current Hand:");
                System.out.println(playersHand.toString());
                System.out.println("Value: " + playersHand.showValue());
                
                //print dealers hand
                System.out.println("Dealer Hand: " + dealersHand.getCard(0).toString() + " & a Hidden Card");
                
                
                //Ask player to hit or stay
                System.out.println("Would you like to (1)Hit or (2)Stay");
                int hitOrStay = kb.nextInt();
                
                //if hit
                if(hitOrStay == 1){
                    playersHand.draw(deck);
                    System.out.println("You draw: " + playersHand.getCard(playersHand.deckSize()-1).toString());
                //BUST
                if(playersHand.showValue() > 21){
                    System.out.println("BUST! Value = " + playersHand.showValue() );
                    money -= playerBet;
                    endofTheRound = true;
                    break;
                }
                }
                //if stay
                if(hitOrStay == 2){
                    break;
                }
            }
            //Dealer Cards
            System.out.println("Dealers Cards were: " + dealersHand.toString());
            
            //Compare dealer cards to player cards.. who won?
            if((dealersHand.showValue() > playersHand.showValue()) && endofTheRound == false){
                System.out.println("Dealer wins.");
                money -= playerBet;
                endofTheRound = true;
            }
            //Dealer hits on 16 and stands on 17
            while((dealersHand.showValue() < 17) && endofTheRound == false){
                dealersHand.draw(deck);
                System.out.println("Dealer draws: " + dealersHand.getCard(dealersHand.deckSize()-1).toString());
            }
            System.out.println("Dealer's Hand Value: " + dealersHand.showValue());
            if((dealersHand.showValue() > 21) && endofTheRound == false){
                System.out.println("Dealer BUSTS! You win!");
                money += playerBet;
                endofTheRound = true;
            }
            else if(endofTheRound == false){
                System.out.println("Dealer Wins.");
                money -= playerBet;
                endofTheRound = true;
                
            }
            
            //If it's a tie
            if((playersHand.showValue() == dealersHand.showValue()) && endofTheRound == false){
                System.out.println("It's a tie! Dealer wins");
                money -= playerBet;
                endofTheRound = true;
            }
            if((playersHand.showValue() > dealersHand.showValue()) && endofTheRound == false){
                System.out.println("You win!");
                money += playerBet;
                endofTheRound = true;
            }
            
            //put cards back into the original deck to play a new round
            playersHand.moveCardsBackToDeck(deck);
            dealersHand.moveCardsBackToDeck(deck);
            System.out.println("End of Round");
        }
        System.out.println("Game Over. You ran out of money!");
        
        
        
    }
    


    
}
